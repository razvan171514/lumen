<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 7/14/2018
 * Time: 12:17 PM
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('users')->insert([
            'user_first_name' => 'admin',
            'user_last_name' => 'admin',
            'user_email' => 'admin@admin.admin',
            'user_password' => app('hash')->make(urldecode('%21%23%2F%29zW%EF%BF%BD%EF%BF%BDC%EF%BF%BDJ%0EJ%EF%BF%BD%1F%EF%BF%BD')),
            'api_token' => 'JxBFpnb1BxjoyRdMoUjIYl0AoaJqeO0kOJmQuYZG3UoSvxg4eBAwfTi7KDgQ',
        ]);
    }

}