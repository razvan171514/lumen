<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 10/15/2018
 * Time: 4:43 PM
 */

class PreferencesTableSeeder extends  Seeder {


    public function run (){
        DB::table('preferences')->insert([
            'archaeology' => '0',
            'architecture' => '0',
            'statues' => '0',
            'memorials' => '0',
            'user_id' => '1',
        ]);
    }

}