<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingNumnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number_of_ratings');
            $table->integer('monument_id')->unsigned();
            $table->foreign('monument_id')->references('id')->on('monuments');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating_numbers');
    }
}
