<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Monument;

class MonumentConrtollerTest extends TestCase {

    use WithoutMiddleware;

    const STATUS_OK = 200;

    /**
     * @test
     * @return void
     */
    public function getMonumentIDByNnameTest() {
        $mounments = Monument::all();
        $baseUrl = '/monument/get/';
        foreach ($mounments as $mounment) {
            $response = $this->json('GET', $baseUrl.urlencode($mounment->name))
                             ->seeStatusCode(self::STATUS_OK);
        }
    }

    /**
     * @test
     * @return void
     */
    public function getMonumentLocationTest() {
        $mounments = Monument::all();
        $baseUrl = '/monument/get-location/';
        foreach ($mounments as $mounment) {
            $response = $this->json('GET', $baseUrl.urlencode($mounment->name))
                             ->seeStatusCode(self::STATUS_OK);
        }
    }
    
    /**
     * @test
     * @return void
     */
    public function getBase64ImageTest() {
        //$mounments = Monument::all();
        $mounments = Monument::where('image_path', '!=', 'https:')
                            ->where('image_path', '!=', '0')
                            ->get();

        dd($mounments);
        $baseUrl = '/monument/get-image/';
        foreach ($mounments as $mounment) {    
            if($mounment->image_path != '0'){
                $reponse = $this->json('GET', $baseUrl.$mounment->id)
                                ->seeStatusCode(self::STATUS_OK);
            }
        }
    }

}
