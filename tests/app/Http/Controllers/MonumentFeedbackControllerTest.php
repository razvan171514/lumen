<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Monument;

class MonumentFeedbackControllerTest extends TestCase {

    use WithoutMiddleware;

    const STATUS_OK = 200;

    /**
     * @test
     * @return void
     */
    public function getAllCommentsTest() {
        $allMonuments = Monument::all();
        $baseUrl = '/feedback/get-all/';

        foreach ($allMonuments as $allMonument) {
            $response = $this->json('GET', $baseUrl.urlencode($allMonument->name))
                             ->seeStatusCode(self::STATUS_OK);
        }
    }

}
