<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'user'], function ($router){
    $router->post('add', 'UserController@create');
    $router->get('all', 'UserController@index');
    $router->get('last', 'UserController@createPreferencesLog');
    $router->get('view/{id}', 'UserController@show');
    $router->get('check/{email}/{password}', 'UserController@getUser');
    $router->group(['middleware' => 'auth'], function ($router){
        $router->get('get-email/{email}', 'UserController@getUserByEmail');
        $router->delete('delete/{id}', 'UserController@destroy');
    });
});

$router->group(['prefix' => 'location'], function ($router){
    $router->post('add', 'LocationController@create');
    $router->get('all', 'LocationController@index');
    $router->get('view/{id}', 'LocationController@show');
    $router->delete('delete/{id}', 'LocationController@destroy');
});

$router->group(['prefix' => 'monument'], function ($router){
    $router->group(['middleware' => 'admin'], function ($router) {
        $router->post('add', 'MonumentController@create');
        $router->delete('delete/{id}', 'MonumentController@destroy');
    });
    $router->group(['middleware' => 'auth'], function ($router) {
        $router->get('all', 'MonumentController@index');
        $router->get('view/{id}', 'MonumentController@show');
        $router->get('get-all', 'MonumentController@getMonumentsForRoute');
        $router->get('get/{name}', 'MonumentController@getMonumentIDByNname');
        $router->get('get-by-preferences' , 'MonumentController@getMonumentsByPreferences');
        $router->get('get-location/{monumentName}', 'MonumentController@getMonumentLocation');
		$router->get('get-image/{id}', 'MonumentController@getBase64Image');
	});
});

$router->group(['prefix' => 'qrcode'], function ($router){
    $router->group(['middleware' => 'admin'], function ($router){
        $router->post('add', 'QrCodeController@create');
        $router->delete('delete/{id}', 'QrCodeController@destroy');
    });
    $router->group(['middleware' => 'auth'], function ($router) {
        $router->get('all', 'QrCodeController@index');
        $router->get('view/{id}', 'QrCodeController@show');
        $router->get('view/get/{code}', 'QrCodeController@getIdByQrCode');
    });
});

$router->group(['prefix' => 'ticket', 'middleware' => 'auth'], function ($router){
    $router->post('add', 'TicketController@create');
    $router->get('all', 'TicketController@index');
    $router->get('all-information/{id}', 'TicketController@getTicketJoinQuery');
    $router->get('view/{id}', 'TicketController@show');
    $router->get('view-all-monuments/{userId}', 'TicketController@getMonumentName');
    $router->delete('delete/{id}', 'TicketController@destroy');
    $router->get('get-ticket-location/{id}', 'TicketController@getTicketWithLocation');

});

$router->group(['prefix' => 'profile', 'middleware' => 'auth'], function ($router){
    $router->post('add', 'ProfileController@create');
    $router->get('all', 'ProfileController@index');
    $router->get('view/{id}', 'ProfileController@show');
    $router->delete('delete/{id}', 'ProfileController@destroy');
});

$router->group(['prefix' => 'language'], function ($router){
    $router->group(['middleware' => 'admin'], function ($router){
        $router->post('add', 'LanguageController@create');
        $router->delete('delete/{id}', 'LanguageController@destroy');
    });
    $router->group(['middleware' => 'auth'], function ($router) {
        $router->get('view/{id}', 'LanguageController@show');
        $router->get('all', 'LanguageController@index');
    });
});

$router->group(['prefix' => 'icon_images', 'middleware' => 'auth'], function ($router){
    $router->post('add', 'IconImageController@create');
    $router->get('all', 'IconImageController@index');
    $router->get('view/{id}', 'IconImageController@show');
    $router->delete('delete/{id}', 'IconImageController@destroy');
});

$router->group(['prefix' => 'monument_type'], function ($router){
    $router->group(['middleware' => 'admin'], function ($router){
        $router->post('add', 'MonumentTypeController@create');
        $router->delete('delete/{id}', 'MonumentTypeController@destroy');
    });
    $router->group(['middleware' => 'admin'], function ($router){
        $router->get('all', 'MonumentTypeController@index');
        $router->get('view/{id}', 'MonumentTypeController@show');
    });
});

$router->group(['prefix' => 'preferences', 'middleware' => 'auth'], function ($router){
    $router->post('add', 'PreferenceController@create');
    $router->get('all', 'PreferenceController@index');
    $router->get('view/{id}', 'PreferencesController@show');
    $router->get('view-user/{id}', 'PreferenceController@getPreferencesId');
    $router->put('update/{id}', 'PreferenceController@update');
    $router->delete('delete/{id}', 'PreferenceController@destroy');
});

$router->group(['prefix' => 'feedback', 'middleware' => 'auth'], function ($router){
    $router->post('add', 'MonumentFeedbackController@create');
    $router->get('all', 'MonumentFeedbackController@index');
    $router->get('view/{id}', 'MonumentFeedbackController@show');
    $router->get('get-all/{monumentName}', 'MonumentFeedbackController@getAllComments');
    $router->delete('delete/{id}', 'MonumentFeedbackController@destroy');
});

$router->group(['prefix' => 'rating', 'middleware' => 'auth'], function ($router){
    $router->post('add', 'RatingController@create');
    $router->get('all', 'RatingController@index');
    $router->get('view/{id}', 'RatingController@show');
    $router->get('view-rating/{userId}/{monumentName}', 'RatingController@getByUserId');
    $router->get('average-rating/{monumentName}', 'RatingController@getAverageRating');
    $router->put('update/{userId}/{monumentId}', 'RatingController@update');
    $router->delete('delete/{id}', 'RatingController@destroy');
});

$router->group(['prefix' => 'user-information'], function ($router){
    $router->group(['middleware' => 'auth'], function($router){
        $router->post('add', 'UsersInformationsController@create');
        $router->get('all', 'UsersInformationsController@index');
        $router->get('view/{id}', 'UsersInformationsController@show');
        $router->get('view-information/{id}', 'UsersInformationsController@getByUserId');
        $router->get('get-image-path/{userId}', 'UsersInformationsController@getPathByUserId');
        $router->delete('delete/{id}', 'UsersInformationsController@destroy');
    });
    $router->get('view-image/{filePath}', 'UsersInformationsController@viewImage');
});

$router->group(['prefix' => 'rating-numbers', 'middleware' => 'auth'], function ($router){
    $router->post('add', 'RatingNumbersController@create');
    $router->group(['middleware' => 'admin'], function ($router){
        $router->post('add-all', 'RatingNumbersController@createTable');
    });
    $router->get('all', 'RatingNumbersController@index');
    $router->get('view/{id}', 'RatingNumbersController@show');
    $router->put('update-increment/{monumentName}', 'RatingNumbersController@updateFromMonumentId');
    $router->delete('delete/{id}', 'RatingNumbersController@destroy');
});

$router->group(['prefix' => 'monument-image'], function ($router){
    $router->group(['middleware' => 'auth'], function ($router){
        $router->post('add', 'MonumentImageController@create');
        $router->get('all', 'MonumentImageController@index');
        $router->get('view/{id}', 'MonumentImageController@show');
        $router->get('view-monument-images/{monumentId}', 'MonumentImageController@getMonumetImages');
        $router->get('view-few-images/{monumentId}', 'MonumentImageController@getFewMonumentImages');
    });
    $router->group(['middleware' => 'admin'], function ($router){
        $router->delete('delete/{id}', 'MonumentImageController@destroy');
    });
    $router->get('view-image/{imagePath}', 'MonumentImageController@getImage');
});

$router->group(['prefix' => 'visited-monuments', 'middleware' => 'auth'], function ($router) {
    $router->post('add', 'VisitedMonumentController@create');
    $router->get('all', 'VisitedMonumentController@index');
    $router->get('get-status', 'VisitedMonumentController@getMonuementVisitedStatus');
    $router->get('get-all-visited-monuments/{userId}', 'VisitedMonumentController@getAllVisitedMonuments');
    $router->get('view/{id}', 'VisitedMonumentController@show');
    //$router->put('update/{userId}/{monumentId}', 'VisitedMonumentController@update');
    $router->delete('delete/{id}', 'VisitedMonumentController@destroy');
});