<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 10/3/2018
 * Time: 9:44 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Rating extends Model {

    /**
     * @var array
     */
    protected $fillable = [
            'rating', 'user_id', 'monument_id'
    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

}