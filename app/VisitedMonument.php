<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitedMonument extends Model {
    
    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'monument_id'
    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

}
