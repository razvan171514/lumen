<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 7/26/2018
 * Time: 10:27 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Monument extends Model {

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'link_wiki', 'rating', 'image_path', 'location_id', 'icon_image_path'
    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

}
