<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 7/26/2018
 * Time: 6:55 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class QrCode extends Model {

    /**
     * @var array
     */
    protected $fillable = [
        'code', 'monument_id',

    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

}