<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preferences extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'archaeology', 'architecture', 'statues', 'memorials', 'user_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
