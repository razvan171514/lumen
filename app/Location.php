<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 7/26/2018
 * Time: 9:25 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Location extends Model {

    /**
     * @var array
     */
    protected $fillable = [
        'longitude', 'latitude',
    ];

    protected $hidden = [

    ];

}