<?php


namespace App\Http\Controllers;


use App\MonumentType;
use Illuminate\Http\Request;


class MonumentTypeController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $type_name = MonumentType::all();

        return response()->json($type_name);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $type_name = MonumentType::create($request->all());

        return response()->json($type_name);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $type_name = MonumentType::find($id);

        return response()->json($type_name);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $id){

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $type_name= MonumentType::find($id);
        $type_name->delete();

        return response()->json($type_name);
    }

}
