<?php

namespace App\Http\Controllers;


use App\Monument;
use App\RatingNumbers;
use Illuminate\Http\Request;


class RatingNumbersController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $rating_numbers = RatingNumbers::all();

        return response()->json($rating_numbers);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $rating_numbers = RatingNumbers::create($request->all());

        return response()->json( $rating_numbers);
    }

    /**
     * @param Request $request
     */
    public function createTable (Request $request){
        ini_set('max_execution_time', 300000);
        $monuments = Monument::all();

        foreach ($monuments as $monument) {
            $request['monument_id'] = $monument->id;
            $request['number_of_ratings'] = 0;

            $ratingNumbers = RatingNumbers::create($request->all());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $rating_numbers = RatingNumbers::find($id);

        return response()->json( $rating_numbers);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $id){

    }

    /**
     * @param $monumentName
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateFromMonumentId ($monumentName){
        $monumentName = urldecode($monumentName);

        $ratingNumbers = RatingNumbers::join('monuments', 'monument_id', '=', 'monuments.id')
            ->where('monuments.name', '=', $monumentName)
            ->select('rating_numbers.*')
            ->first();

        $ratingNumbers->number_of_ratings = $ratingNumbers->number_of_ratings + 1;
        $ratingNumbers->save();

        return response()->json($ratingNumbers);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $rating_numbers = RatingNumbers::find($id);
        $rating_numbers->delete();

        return response()->json($rating_numbers);
    }

}