<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 9/14/2018
 * Time: 4:49 PM
 */

namespace App\Http\Controllers;

use App\Preferences;
use Illuminate\Http\Request;

class PreferenceController extends Controller {
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $type_name = Preferences::all();

        return response()->json($type_name);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $type_name = Preferences::create($request->all());

        return response()->json($type_name);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $type_name = Preferences::find($id);

        return response()->json($type_name);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPreferencesId ($id) {
        $preference = Preferences::where('user_id', $id)
                                 ->get();

        return response()->json($preference);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update (Request $request, $id){
        $preferences=Preferences::find($id);

        $preferences->archaeology = $request->input('archaeology');
        $preferences->architecture = $request->input('architecture');
        $preferences->statues = $request->input('statues');
        $preferences->memorials = $request->input('memorials');
        $preferences->user_id = $request->input('user_id');

        $preferences-> save();

        return response()->json($preferences);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $type_name= Preferences::find($id);
        $type_name->delete();

        return response()->json($type_name);
    }
}
