<?php

namespace App\Http\Controllers;


use App\IconImage;
use Illuminate\Http\Request;


class IconImageController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $IconImage = IconImage::all();

        return response()->json($IconImage);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $IconImage = IconImage::create($request->all());

        return response()->json($IconImage);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $IconImage = IconImage::find($id);

        return response()->json($IconImage);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $id){

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $IconImage =IconImage::find($id);
        $IconImage ->delete();

        return response()->json($IconImage);
    }

}