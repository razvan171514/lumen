<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 10/3/2018
 * Time: 9:42 PM
 */

namespace App\Http\Controllers;

use App\User;
use App\Rating;
use App\UsersInformations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Validator;

class UsersInformationsController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $ratings = UsersInformations::all();

        return response()->json($ratings);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $validator = Validator::make($request->all(), [
            'image' => 'required',
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response('Invalid request.', 400);
        }

        $information = UsersInformations::where('user_id', $request->input('user_id'))->first();
        if (!is_null($information)) {
            $this->update($request, $request->input('user_id'));
            return response()->json($information);
        }

        $file = $request->file('image');
        $fileName = Storage::disk('local')->putFile('user_profile_images', new \Illuminate\Http\File($file));
        $usersInformation = UsersInformations::create([
            'image_path' => $fileName,
            'user_id' => $request->input('user_id')
        ]);
        return response()->json($usersInformation);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $ratings = UsersInformations::find($id);

        return response()->json($ratings);
    }

    /**
     * @param $filePath
     * @return \Illuminate\Http\Response
     */
    public function viewImage($filePath){
        $filePath = base64_decode(urldecode($filePath));
        $filePath = str_replace('%2F', '/', $filePath);
        if (Storage::has($filePath)) {
            $file = Storage::disk('local')->get($filePath);
            return response($file, 200);
        }else {
            return response('Not found.', 404);
        }
    }
    
    /**
     * @param $userId
     * @return \Illuminate\Http\Response
     */
    public function getPathByUserId($userId) {
        $userInformation = UsersInformations::where('user_id', $userId)
            ->select('users_informations.id', 'users_informations.image_path', 'users_informations.user_id')
            ->first();

        return response()->json($userInformation);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByUserId ($id){
        $usersInformation = UsersInformations::join('users', 'user_id', '=', 'users.id')
                                    ->select('users_informations.*', 'users.user_first_name', 'users.user_last_name')
                                    ->where('users.id', $id)
                                    ->orderBy('users_informations.id', 'DESC')
                                    ->get();

        return response()->json($usersInformation);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $userId) {
        $usersInformation = UsersInformations::where('user_id', $userId)
                ->first();

        $file = $request->file('image');
        Storage::disk('local')->delete($usersInformation->image_path);
        $fileName = Storage::disk('local')->putFile('user_profile_images', new \Illuminate\Http\File($file));
        $usersInformation->image_path = $fileName;
        $usersInformation->save();

        return response()->json($usersInformation);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy (Request $request, $id){
        $user = User::where('api_token', $request->header('api_token'))->first();
        $userInformation = UsersInformations::find($id);

        if ($user->id != $userInformation->user_id) {
            return response('Forbidden.', 403);
        }

        Storage::disk('local')->delete($userInformation->image_path);
        $userInformation->delete();
        return response()->json($userInformation);
    }

}