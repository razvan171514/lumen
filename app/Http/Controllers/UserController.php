<?php

namespace App\Http\Controllers;


use App\Preferences;
use App\User;
use App\UsersInformations;
use App\MonumentFeedback;
use App\Ticket;
use App\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Database\QueryException;

class UserController extends Controller {

    //$initVector = '';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $user = User::all();

        return response()->json($user);
    }

    /**
     *  Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request){
        $request['user_password'] = app('hash')->make(urldecode($request['user_password']));
        $request['api_token'] = str_random(60);

        try {
            $user = User::create($request->all())->id;
        }catch (Exception $exception) {
            if ($exception instanceof QueryException) {
                return response($exception, 409);
            }
        }

        $requestPrefs = new Request();

        $requestPrefs['archaeology'] = 0;
        $requestPrefs['architecture'] = 0;
        $requestPrefs['statues'] = 0;
        $requestPrefs['memorials'] = 0;
        $requestPrefs['user_id'] = $user;

        $preference = Preferences::create($requestPrefs->all());

        return response()->json($preference);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     */
    public function store(Request $request){

    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        $user = User::find($id);

        return response()->json($user);
    }

    /**
     * @param $email
     * @param $password
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser ($email, $password){
        $base64DecodedEmailAdress = base64_decode ($email);
        $user = User::where('user_email', '=', $base64DecodedEmailAdress)->first();

        if (Hash::check(urldecode($password), $user['user_password']))
            return response()->json($user);
        else return response()->json(null);
    }

    /**
     * @param $email
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserByEmail($email) {
        $base64DecodedEmailAdress = base64_decode($email);
        $user = User::where('user_email', '=', $base64DecodedEmailAdress)
                    ->select('users.id', 'users.user_first_name', 'users.user_last_name', 'users.user_email', 'users.created_at', 'users.updated_at')
                    ->first();

        return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     */
    public function edit($id){

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id){

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        $user = User::find($id);
        $userPreferences = Preferences::where('user_id', $id)->first();
        $userProfileInformations = UsersInformations::where('user_id', $id)->get();
        $userFeedbacks = MonumentFeedback::where('user_id', $id)->get();
        $userTickets = Ticket::where('user_id', $id)->get();
        $userRatings = Rating::where('user_id', $id)->get();

        try {
            foreach ($userProfileInformations as $userProfileInformation) {
                $userProfileInformation->delete();
            }
            foreach ($userFeedbacks as $userFeedback) {
                $userFeedback->delete();
            }
            foreach ($userTickets as $userTicket) {
                $userTicket->delete();
            }
            foreach ($userRatings as $userRating) {
                $userRating->user_id = null;
                $userRating->save();
            }
            $userPreferences->delete();
            $user->delete();
        }catch (Exception $exception) {
            if ($exception instanceof QueryException) {
                return response($exception, 409);
            }
        }

        return response()->json($user);
    }

}
