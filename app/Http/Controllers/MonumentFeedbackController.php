<?php


namespace App\Http\Controllers;


use App\MonumentFeedback;
use App\UsersInformations;
use Illuminate\Http\Request;


class MonumentFeedbackController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $feedback = MonumentFeedback::all();

        return response()->json($feedback);
    }

    /**
     * @param $monumentName
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllComments($monumentName){
        $monumentName = urldecode($monumentName);

        $comments = MonumentFeedback::join('users', 'user_id', '=', 'users.id')
                                    ->join('monuments', 'monument_id', '=', 'monuments.id')
                                    ->select('monument_feedbacks.id as comment_id', 'monument_feedbacks.comment', 'users.user_first_name', 'users.id', 'users.user_last_name', 'monuments.name')
                                    ->where('monuments.name', $monumentName)
                                    ->orderBy('monument_feedbacks.created_at', 'DESC')
                                    ->get();

        $commentList = array();
        foreach ($comments as $comment) {
            $userProfileImage = UsersInformations::where('user_id', $comment->id)->first();
            array_push(
                $commentList, 
                array(
                    'comment_id' => $comment->comment_id,
                    'comment' => $comment->comment,
                    'user_first_name' => $comment->user_first_name,
                    'user_last_name' => $comment->user_last_name,
                    'name' => $comment->name,
                    'id' => $comment->id,
                    'image_path' => $userProfileImage['image_path']
                )
            );
        }

        return response()->json($commentList);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $feedback = MonumentFeedback::create($request->all());

        return response()->json($feedback);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $feedback = MonumentFeedback::find($id);

        return response()->json($feedback);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $id){

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $feedback= MonumentFeedback::find($id);
        $feedback->delete();

        return response()->json($feedback);
    }

}
