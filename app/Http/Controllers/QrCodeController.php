<?php


namespace App\Http\Controllers;


use App\QrCode;
use Illuminate\Http\Request;

class QrCodeController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $qrCode = QrCode::all();

        return response()->json($qrCode);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $qrCode = QrCode::create($request->all());

        return response()->json($qrCode);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $qrCode = QrCode::find($id);

        return response()->json($qrCode);
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getIdByQrCode($code){
        $qrCodeId = QrCode::select('id', 'monument_id')->where('code', $code)->get();

        return response()->json($qrCodeId);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $id){

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $qrCode = QrCode::find($id);
        $qrCode->delete();

        return response()->json($qrCode);
    }

}
