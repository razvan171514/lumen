<?php

namespace App\Http\Controllers;


use App\Language;
use Illuminate\Http\Request;


class LanguageController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $language = Language::all();

        return response()->json($language);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $language = Language::create($request->all());

        return response()->json($language);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $language = Language::find($id);

        return response()->json($language);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $id){

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $language =Language::find($id);
        $language->delete();

        return response()->json($language);
    }

}