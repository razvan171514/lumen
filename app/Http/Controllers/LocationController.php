<?php

namespace App\Http\Controllers;


use App\Location;
use Illuminate\Http\Request;


class LocationController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $location = Location::all();

        return response()->json($location);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $location = Location::create($request->all());

        return response()->json($location);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $location = Location::find($id);

        return response()->json($location);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $id){

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $location = Location::find($id);
        $location->delete();

        return response()->json($location);
    }

}
