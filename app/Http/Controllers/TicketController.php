<?php

namespace App\Http\Controllers;


use App\Ticket;
use Illuminate\Http\Request;


class TicketController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $ticket = Ticket::all();

        return response()->json($ticket);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTicketJoinQuery ($id){
        $ticket = Ticket::join('qr_codes', 'qr_code_id', '=', 'qr_codes.id')
                        ->join('monuments', 'tickets.monument_id', '=', 'monuments.id')
                        ->select('tickets.id', 'tickets.user_id' ,'tickets.date',  'qr_codes.code', 'monuments.name')
                        ->where('tickets.user_id', '=', $id)
                        ->get();

        return response()->json($ticket);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTicketWithLocation ($id){
        $ticket = Ticket::join('qr_codes', 'qr_code_id', '=', 'qr_codes.id')
            ->join('monuments', 'tickets.monument_id', '=', 'monuments.id')
            ->join('locations', 'monuments.location_id', '=', 'locations.id')
            ->select('tickets.id', 'tickets.user_id' ,'tickets.date',  'qr_codes.code', 'monuments.name', 'locations.longitude', 'locations.latitude')
            ->where('tickets.user_id', '=', $id)
            ->get();

        return response()->json($ticket);
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMonumentName ($userId){
        $ticket = Ticket::join('monuments', 'monument_id', '=', 'monuments.id')
                        ->join('locations', 'monuments.location_id', '=', 'locations.id')
                        ->select('tickets.*', 'monuments.name', 'monuments.location_id', 'locations.longitude', 'locations.latitude')
                        ->where('tickets.user_id', $userId)
                        ->get();

        return response()->json($ticket);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $ticket = Ticket::create($request->all());

        return response()->json($ticket);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $ticket = Ticket::find($id);

        return response()->json($ticket);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $id){

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $ticket = Ticket::find($id);
        $ticket->delete();

        return response()->json($ticket);
    }

}