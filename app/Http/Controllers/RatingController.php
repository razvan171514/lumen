<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 10/3/2018
 * Time: 9:42 PM
 */

namespace App\Http\Controllers;


use App\Rating;
use App\RatingNumbers;
use Illuminate\Http\Request;

class RatingController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $ratings = Rating::all();

        return response()->json($ratings);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $rating = Rating::where('user_id', $request->input('user_id'))
                        ->where('monument_id', $request->input('monument_id'))
                        ->first();

        if ($rating != null) {
            $this->update($request, $request->input('user_id'), $request->input('monument_id'));
            return response()->json($rating);
        }

        $ratings = Rating::create($request->all());

        return response()->json($ratings);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $ratings = Rating::find($id);

        return response()->json($ratings);
    }

    /**
     * @param $userId
     * @param $monumentName
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByUserId ($userId, $monumentName){
        $monumentName = urldecode($monumentName);

        $rating = Rating::join('monuments', 'monument_id', '=', 'monuments.id')
            ->where('monuments.name', '=', $monumentName)
            ->where('user_id',  $userId)
            ->select('ratings.*')
            ->get();

        return response()->json($rating);
    }

    /**
     * @param $monumentName
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAverageRating ($monumentName){
        $monumentName = urldecode($monumentName);

        $ratings = Rating::join('monuments', 'monument_id', '=', 'monuments.id')
            ->select('ratings.*')
            ->where('monuments.name', '=', $monumentName)
            ->get();

        $averageRating = 0;
        foreach ($ratings as $rating) {
            $averageRating += $rating->rating;
        }

        $ratingNumber = RatingNumbers::join('monuments', 'monument_id', '=', 'monuments.id')
            ->select('rating_numbers.*')
            ->where('monuments.name', $monumentName)
            ->first();

        if ($ratingNumber->number_of_ratings) {
            $averageRating = $averageRating / $ratingNumber->number_of_ratings;
        }
        else {
            $averageRating = 0;
        }

        $averageRating = round($averageRating, 1, PHP_ROUND_HALF_UP);

        return response()->json(array('average_rating' => $averageRating));
    }

    public static function staticAverageRating($monumentName) {
        return (new self)->getAverageRating(urldecode($monumentName))->original['average_rating'];
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $userId, $monumentId){
        $ratings = Rating::where('monument_id', $monumentId)
                        ->where('user_id', $userId)
                        ->first();

        $ratings->rating = $request->input('rating');

        $ratings->save();

        return response()->json($ratings);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $rating = Rating::find($id);
        $rating->delete();

        return response()->json($rating);
    }

}