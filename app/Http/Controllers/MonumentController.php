<?php


namespace App\Http\Controllers;


use App\Monument;
use App\Preferences;
use Illuminate\Http\Request;
use App\MonumentType;
use App\VisitedMonument;
use GeometryLibrary\PolyUtil;
use App\Http\Controllers\RatingController;

class MonumentController extends Controller {

    const DEFAULT_TOLERANCE = 5000;
    const DEFAULT_TRAVEL_MODE = 'driving';

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $monument = Monument::all();

        return response()->json($monument);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMonumentsForRoute (){
        $monument = Monument::join('locations', 'location_id', '=', 'locations.id')
                            ->select('monuments.*', 'locations.longitude', 'locations.latitude')
                            ->orderBy('monuments.rating', 'DESC')
                            ->take(11)
                            ->get();

        return response()->json($monument);
    }
    /**
     * @param Preferences $preferences
     * @return bool
     */
    public function checkPreferences(Preferences $preferences) {
        return (($preferences->archaeology==1) || 
                ($preferences->architecture==1) || 
                ($preferences->statues==1) || 
                ($preferences->memorials==1));
    }

    /**
     * @param $mapRequestUrl
     * @return mixed
     */
    public function sendMapRequest($mapRequestUrl){
        return json_decode(file_get_contents($mapRequestUrl));
    }

    /**
     * @param $jsonRoute
     * @return array
     */
    public function generateRouteGeometry($jsonRoute) {
        $route = array();
        $routeCoordinates = $jsonRoute->routes[0]->geometry->coordinates;
        foreach ($routeCoordinates as $coordinate) {
            $element = [
                'lat' => $coordinate[0],
                'lng' => $coordinate[1]
            ];
            array_push($route, $element);
        }
        return $route;
    }

    /**
     * @param $orogin
     * @param $desination
     * @param null $travelMode
     * @param bool $withWaypoints
     * @param null $waypoits
     * @return string
     */
    public function generateMapboxApiUrl($orogin, $desination, $travelMode=null, $withWaypoints=false, $waypoits=null){
        $baseUrl = 'https://api.mapbox.com/directions/v5/mapbox/';
        $originUrl = $orogin['lat'].','.$orogin['lng'].';';
        $desinationUrl = $desination['lat'].','.$desination['lng'];
        $travelModeUrl = ($travelMode === null) ? self::DEFAULT_TRAVEL_MODE : strtolower($travelMode);
        $stepsUrl = 'steps=true';
        $alternativeRoutesUrl = 'alternatives=false';
        $geometriesUrl = 'geometries=geojson';
        $apiTokenUrl = 'access_token=pk.eyJ1IjoibXVzZWVjb21wYW55IiwiYSI6ImNqcDJ4ODYzNzAwdnYzcG85ZWJiZ2gxN2sifQ.6B8XBCtOnCnIhCb2AzoY0w';
        $urlParameters = $travelModeUrl.'/'.$originUrl;
        if ($withWaypoints) {
            foreach ($waypoits as $waypoit) {
                $urlParameters = $urlParameters.$waypoit['lat'].','.$waypoit['lng'].';';
            }
        }
        $urlParameters = $urlParameters.$desinationUrl.'?'.$geometriesUrl.'&'.$stepsUrl.'&'.$alternativeRoutesUrl.'&'.$apiTokenUrl;
        return $baseUrl.$urlParameters;
    }

    /**
     * @param $jsonResponse
     * @return array|null
     */
    public function parseJsonRoute($jsonResponse){
        $route = array();
        if ($jsonResponse->code != 'Ok') {
            return null;
        }
        $jsonLegs = $jsonResponse->routes[0]->legs;
        foreach ($jsonLegs as $jsonLeg) {
            $jsonSteps = $jsonLeg->steps;
            foreach ($jsonSteps as $jsonStep) {
                $jsonStepGeometryCoordinates = $jsonStep->geometry->coordinates;
                foreach ($jsonStepGeometryCoordinates as $jsonStepGeometryCoordinate) {
                    array_push($route, [
                        'lat' => $jsonStepGeometryCoordinate[1],
                        'lng' => $jsonStepGeometryCoordinate[0]
                    ]);
                }
            }
        }
        return $route;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMonumentsByPreferences(Request $request){
        
        $basePolyline = $this->generateRouteGeometry($this->sendMapRequest($this->generateMapboxApiUrl(
            ['lat' => $request->header('origin_lat'), 'lng' => $request->header('origin_lng')],
            ['lat' => $request->header('destination_lat'), 'lng' => $request->header('destination_lng')],
            $travelMode = $request->header('travel_mode')
        )));

        $monumentTypes=MonumentType::join('monuments', 'monument_id', '=', 'monuments.id')
                                    ->join('locations', 'monuments.location_id', '=', 'locations.id')
                                    ->select('monument_types.type_name', 'monuments.*', 'locations.longitude', 'locations.latitude')
                                    ->orderByRaw("RAND()")
                                    ->get();

        //return response()->json($monumentTypes);

        $preferences=Preferences::where('user_id', $request->header('user_id'))
                                ->get();

        $foundMonumentsArray = array();
        $foundMonumentLocationsArray = array();

        if ($this->checkPreferences($preferences[0]) === false) {
            foreach ($monumentTypes as $monumentType) {
                if (count($foundMonumentsArray) === 11) {
                    break;
                }elseif (PolyUtil::isLocationOnPath(['lat' => $monumentType->latitude, 'lng' => $monumentType->longitude], $basePolyline,
                                $tolerance = ($request->header('road_tolerance') != 0) ? $request->header('road_tolerance')*1000 : self::DEFAULT_TOLERANCE)) {
                    array_push($foundMonumentsArray, $monumentType);
                    array_push($foundMonumentLocationsArray, [
                        'lat' => $monumentType->latitude,
                        'lng' => $monumentType->longitude,
                        'name' => $monumentType->name
                    ]);
                }
            }
        }else {
            $iter=0;
            foreach ($monumentTypes as $monumentType){
                if ($iter === 11) {
                    break;
                }
                $name = $monumentType->type_name;

                $visitedMonument = VisitedMonument::join('monuments', 'monuments.id', '=', 'monument_id')
                    ->select('visited_monuments.*')
                    ->where('monuments.name', '=', $name)
                    ->where('user_id', $request->header('user_id'))
                    ->first();

                $monumentLocation = [
                    'lat' => $monumentType->latitude,
                    'lng' => $monumentType->longitude
                ];
                if (PolyUtil::isLocationOnPath($monumentLocation, $basePolyline,
                                    $tolerance = ($request->header('road_tolerance') != 0) ? $request->header('road_tolerance')*1000 : self::DEFAULT_TOLERANCE)) {
                    switch($name){
                        case "Monumente de arheologie":
                            if ($preferences[0]->archaeology==1) {
                                $foundMonumentsArray[$iter++]=$monumentType;
                                array_push($foundMonumentLocationsArray, [
                                    'lat' => $monumentType->latitude,
                                    'lng' => $monumentType->longitude,
                                    'monument_id' =>$monumentType->id,
                                    'name' => $monumentType->name,
                                    'image_url' => $monumentType->image_path,
                                    'average_rating' => RatingController::staticAverageRating($monumentType->name),
                                    'is_visited' => (is_null($visitedMonument) ? false : true)
                                ]);
                            }
                            break;
                        case "Monumente arhitectură":
                            if ($preferences[0]->architecture==1) {
                                $foundMonumentsArray[$iter++]=$monumentType;
                                array_push($foundMonumentLocationsArray, [
                                    'lat' => $monumentType->latitude,
                                    'lng' => $monumentType->longitude,
                                    'monument_id' =>$monumentType->id,
                                    'name' => $monumentType->name,
                                    'image_url' => $monumentType->image_path,
                                    'average_rating' => RatingController::staticAverageRating($monumentType->name),
                                    'is_visited' => (is_null($visitedMonument) ? false : true)
                                ]);
                            }
                            break;
                        case "Monumente de for public":
                            if ($preferences[0]->statues==1) {
                                $foundMonumentsArray[$iter++]=$monumentType;
                                array_push($foundMonumentLocationsArray, [
                                    'lat' => $monumentType->latitude,
                                    'lng' => $monumentType->longitude,
                                    'monument_id' =>$monumentType->id,
                                    'name' => $monumentType->name,
                                    'image_url' => $monumentType->image_path,
                                    'average_rating' => RatingController::staticAverageRating($monumentType->name),
                                    'is_visited' => (is_null($visitedMonument) ? false : true)
                                ]);
                            }
                            break;
                        case "Monumente memoriale și funerare":
                            if ($preferences[0]->memorials==1) {
                                $foundMonumentsArray[$iter++]=$monumentType;
                                array_push($foundMonumentLocationsArray, [
                                    'lat' => $monumentType->latitude,
                                    'lng' => $monumentType->longitude,
                                    'monument_id' =>$monumentType->id,
                                    'name' => $monumentType->name,
                                    'image_url' => $monumentType->image_path,
                                    'average_rating' => RatingController::staticAverageRating($monumentType->name),
                                    'is_visited' => (is_null($visitedMonument) ? false : true)
                                ]);
                            }
                            break;
                        /*default:
                            $foundMonumentsArray[$iter++]=$monumentType;
                            break;*/
                    }
                }
            }
        }

        sort($foundMonumentLocationsArray);
        $route = $this->parseJsonRoute($this->sendMapRequest($this->generateMapboxApiUrl(
            ['lat' => $request->header('origin_lat'), 'lng' => $request->header('origin_lng')],
            ['lat' => $request->header('destination_lat'), 'lng' => $request->header('destination_lng')],
            $travelMode = $request->header('travel_mode'),
            $withWaypoints = true, $foundMonumentLocationsArray
        )));

        return response()->json(array($route, $foundMonumentLocationsArray));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
	public function getBase64Image($id){
        $monuments=Monument::findOrfail($id);
        if ($monuments->image_path === '0') {
            return response('Not Found.', 404);
        }
		return response()->json(array('image_path' => $monuments->image_path));
	}
    
    /**
     * @param $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMonumentIDByNname($name){
        //$name = base64_decode(urldecode($name));
        $name = urldecode($name);
        $monument = Monument::where('name', $name)->get();

        return response()->json($monument);
    }

    /**
     * @param $monumentName
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMonumentLocation($monumentName){
        $monumentName = urldecode($monumentName);
        //TODO: change migration file
        $monument = Monument::join('locations', 'location_id', '=', 'locations.id')
                            ->select('monuments.id', 'monuments.name', 'locations.longitude', 'locations.latitude')
                            ->where('monuments.name', $monumentName)
                            ->get();

        return response()->json($monument);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $monument = Monument::create($request->all());

        return response()->json($monument);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $monument = Monument::find($id);

        return response()->json($monument);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $id){


    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $monument = Monument::find($id);
        $monument->delete();

        return response()->json($monument);
    }

}
