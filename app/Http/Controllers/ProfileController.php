<?php

namespace App\Http\Controllers;


use App\Profile;
use Illuminate\Http\Request;


class ProfileController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $profile = Profile::all();

        return response()->json($profile);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $profile = Profile::create($request->all());

        return response()->json($profile);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $profile = Profile::find($id);

        return response()->json($profile);
    }

    /**
     * @param $id
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update (Request $request, $id){

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy ($id){
        $profile = Profile::find($id);
        $profile->delete();

        return response()->json($profile);
    }

}