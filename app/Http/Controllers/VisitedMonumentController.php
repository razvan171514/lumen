<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 10/3/2018
 * Time: 9:42 PM
 */

namespace App\Http\Controllers;

use App\User;
use App\Monument;
use App\VisitedMonument;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Log;
use Illuminate\Database\QueryException;

class VisitedMonumentController extends Controller {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (){
        $visitedMonuments = VisitedMonument::all();
        return response()->json($visitedMonuments);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'monument_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response('Invalid request.', 400);
        }

        $monument = VisitedMonument::where([
                ['user_id', '=', $request->input('user_id')],
                ['monument_id', '=', $request->input('monument_id')],
            ])->exists();
        
        if ($monument) {
            return $this->destroy($request, $monument->id);
        } else {
            try {
                $visitedMonument = VisitedMonument::create($request->all());
            } catch (Exception $exception) {
                if ($exception instanceof QueryException) {
                    Log::error($exception);
                    return response($exception, 501);
                }
            }
            return response()->json($visitedMonument);
        }
        
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show ($id){
        $visitedMonument = VisitedMonument::find($id);
        return response()->json($visitedMonument);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMonuementVisitedStatus(Request $request) {
        $visitedMonument = VisitedMonument::where('user_id', $request->header('user_id'))
            ->where('monument_id', $request->header('monument_id'))
            ->first();
        if (is_null($visitedMonument)) {
            return response()->json(array('status' => false));
        }else {
            return response()->json(array('status' => true));
        }
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllVisitedMonuments($userId) {
        $visitedMonuments = VisitedMonument::join('monuments', 'monument_id', '=', 'monuments.id')
            ->join('locations', 'monuments.location_id', '=', 'locations.id')
            ->select(/*'visited_monuments.*',*/ 'locations.latitude', 'locations.longitude', 'monuments.name')
            ->where('user_id', $userId)
            ->get();

        return response()->json($visitedMonuments);
    }

    /**
     * @param $id
     */
    public function edit ($id){
        
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id) {
        
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @return \Illuminate\Http\Response
     */
    public function destroy (Request $request, $id){
        $visitedMonument = VisitedMonument::find($id);
        try {
            $visitedMonument->delete();
        }catch (Exception $exception) {
            if ($exception instanceof QueryException) {
                return response('Query exception', 500);
            }
        }
        return response()->json(array('deleted' => true));
    }

}