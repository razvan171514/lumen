<?php

namespace App\Http\Controllers;

use App\MonumentImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MonumentImageController extends Controller {
    
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $monumentImages = MonumentImage::all();
        
        return response()->json($monumentImages);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'image' => 'required',
        ]);
        if ($validator->fails()) {
            return response('Invalid request.', 400);
        }

        $file = $request->file('image');
        $fileName = Storage::disk('local')->putFile('monument_images', new \Illuminate\Http\File($file));
        $monumentImage = MonumentImage::create([
            'image_path' => $fileName,
            'monument_id' => $request->header('monument_id'),
            'user_id' => $request->header('user_id')
        ]);
        return response()->json($monumentImage);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id) {
        $monumentImage = MonumentImage::find($id);

        return response()->json($monumentImage);
    }

    /**
     * @param $imagePath
     * @return \Illuminate\Http\Response
     */
    public function getImage($imagePath) {
        $imagePath  = base64_decode(urldecode($imagePath));
        $imagePath = str_replace('%2F', '/', $imagePath);
        if (Storage::has($imagePath)) {
            $file = Storage::disk('local')->get($imagePath);
            return response($file, 200);
        }else  {
            return response('Not foud.', 404);
        }
    }

    /**
     * @param @monumentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMonumetImages($monumentId){
        $monumentImages = MonumentImage::where('monument_id', $monumentId)
            ->select('monument_images.id', 'monument_images.image_path', 'monument_images.monument_id', 'monument_images.user_id')
            ->get();
        return response()->json($monumentImages);
    }

    /**
     * @param @monumentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFewMonumentImages($monumentId) {
        $monumentImages = MonumentImage::where('monument_id', $monumentId)
           ->take(10)
           ->get();
        return response()->json($monumentImages);
    }

    /**
     * @param $id
     * Unrouted.
     */
    public function edit ($id){

    }

    /**
     * @param Request $request
     * @param $id       
     * Unrouted.
     */
    public function update (Request $request, $id){

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {
        $monumentImage = MonumentImage::find($id);
        $monumentImage->delete();

        return response()->json($monumentImage);
    }

}
