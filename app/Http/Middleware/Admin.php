<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 3/14/2019
 * Time: 1:36 PM
 */

namespace App\Http\Middleware;

use Closure;
use App\User;

class Admin {

    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory|mixed
     */
    public function handle($request, Closure $next) {
        $admin = User::where('api_token', '=', 'JxBFpnb1BxjoyRdMoUjIYl0AoaJqeO0kOJmQuYZG3UoSvxg4eBAwfTi7KDgQ')
            ->first();

        if ($request->header('api_token') != $admin->api_token) {
            return response('Unauthorised.', 401);
        }

        return $next($request);
    }

}