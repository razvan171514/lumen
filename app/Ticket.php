<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'monument_id', 'user_id', 'qr_code_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
