<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class IconImage extends Model {

    /**
     * @var array
     */
    protected $fillable = [
         'icon_image_path'
    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

}
