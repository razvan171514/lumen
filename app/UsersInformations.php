<?php
/**
 * Created by PhpStorm.
 * User: Razvan
 * Date: 10/10/2018
 * Time: 5:01 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class UsersInformations extends Model {

    /**
     * @var array
     */
    protected $fillable = [
        'image_path', 'user_id'
    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

}